# [xmpp 教學] Ios 篇

因為我並沒有Iphone，xmpp/ios 我可以提供的資訊會比較侷限一些，底下的步驟大多是參考(聽說)其他人的文章之後，整理出來的。

建議看完[xmpp 簡介](https://codeberg.org/explorer422/phone_book/src/branch/main/tutorial/xmpp_%E7%B0%A1%E4%BB%8B.md)，有些基本背景再過來看這篇文章，不過其中的大原則是不變的。 
 
## 前置作業
- 一個xmpp帳號 [如何申請](https://codeberg.org/explorer422/phone_book/src/branch/main/tutorial/xmpp_%E7%B0%A1%E4%BB%8B.md#%E7%94%B3%E8%AB%8Bxmpp%E5%B8%B3%E8%99%9F)
- ChatSecure [官網](https://chatsecure.org/) [App Store 連結](https://itunes.apple.com/us/app/chatsecure/id464200063)

### 使用說明

#### 中文
> 其他網友的教學文章
- https://self.jxtsai.info/2016/05/chatsecure.html

#### 英文

> ChatSecure 官方OMEMO說明
- https://chatsecure.org/blog/chatsecure-v4-released/

如同上述，因為我沒有Iphone，不能詳細使用過程寫出來(罪該萬死)，不過重要的是到最後要確定底下的這個圖

這張就是在ChatSecure裡面的OMEMO鑰匙管理頁面。

<img src="https://chatsecure.org/images/v4-profile.png" width="300">



### 核對OMEMO鑰匙
當你已經把應用程式裝好，並依照上面的文章把一些設定完成之後，接下來就是最重要的部分: **核對鑰匙** 。

**加入聯絡人之後一定要確認裝置上出現的鑰匙是別人的**，除了可以親自面對面核對鑰匙之外，不然就是核對他們公開在網路上的鑰匙。

以下是我的OMEMO鑰匙:

`Laptop: d09de4b7 71a5acfe 331894ba eabcc33e  284671c3 430903f1  e8e72451 c1797806`
  
`Cellphone: 8f7b2eeb 028b4923 89552129 bfc962d5 67d15697 b9963d80 e8031134 cea93921`

**一定要確認這兩把鑰匙都是正確的**

## 結語。
讀到這裡表示你已經可以使用xmpp跟其他人聯繫了，給自己一點鼓勵吧。如果還有什麼沒有搞得很懂得歡迎再聯絡我，或是在這個repo裡面開issue也是可以(我會很開心的，哈哈)。最後，歡迎加入xmpp大家庭!!
