# 什麼是xmpp？
> xmpp is an open communication protocol designed for instant messaging _[Wikipedia]_

# 不同類型的網絡
<img src="https://www.researchgate.net/profile/Yousef_Darmani/publication/309327929/figure/download/fig1/AS:669218538270744@1536565541903/I-Centralized-II-Decentralized-and-III-Distributed-Networks.jpg" width="300">

### (I) Centralized Network
最常見的網絡形式。LINE、Facebook Messenger、Whatsapp...都是Centralized Network，由中心的伺服器管理所有的訊息傳遞。

好處就是大家只要去一個地方註冊、使用。

不過相對應的，壞處就是管理圖中最中心點的那個(群)人，可以

#### Example:
我今天傳Facebook Messenger的訊息給住在台北的蔡英文，訊息只會經過Facebook的伺服器，最後蔡英文的裝置會顯示我傳訊息給她。

### (II) Decentralized Network
xmpp使用的就是這種網絡形式，常見的包含大家常用的email。運作原理是透過不同分散式的點，來傳遞訊息。

#### Example:
今天Alice從alice@tutanota.com寄信給她的同事，使用bob@protonmail.com的Bob，當電子郵件從Alice送出時，首先由tutanota的伺服器收到這個信件，之後再傳遞到protonmail的伺服器，最後Bob才會收到信件。

### (III) Distributed(Peer-to-Peer or P2P) Network
如果有看過第一版的"_沒有電話的電話簿_"的人，應該有看過Briar或是Jami，這兩個應用程式應用的就是P2P。

這個網絡的好處就是，沒有人有全權管控整個網絡，大家都是一樣重要的，因此當一個人(或是伺服器)無法連上線的時候，訊息的流通只會稍微(甚至不會)受到影響。

# 申請xmpp帳號
如同前面有提到的xmpp 跟email 都是一種open protocal。也就是說，你可以不用跟我選一樣的，只要找到自己喜歡的伺服器註冊，都可以跟其他人聯繫。

我自己個人是用 [xmpp.is](https://xmpp.is/)

公開的xmpp伺服器:
- [xmpp-server.404.city](https://xmpp-servers.404.city/)
- [xmpp.net](https://www.xmpp.net/directory.php)

## 申請帳號時要注意的事項
!! 一定要看Privacy Policy

!! 一定要看Privacy Policy

!! 一定要看Privacy Policy

當今幾乎所有的網站都有俗稱隱私權政策的Privacy Policy，一份好的Privacy Policy應該寫到他們怎麼處理使用者的資料、使用者可主張什麼資訊權力(Digital Rights)等等。

雖然你能母胎之後就從來沒有看過什麼網站的隱私權政策，不過我強烈建議你在申請帳號之前看過一次，如果裡面也有提到一些你無法接受的事項，非常簡單，換一個你覺得信任的地方註冊，又或者，你可以選擇自架，這樣你就幾乎有全權掌握你的資訊了。

部分節錄xmpp.is的privacy policy:
> We do NOT sell or use any user data from XMPP.is for commercial purposes.

> We do NOT store user’s IP addresses. The only exception is when debug logs are temporarily enabled to troubleshoot an issue with the server. After the logging is disabled, the log file is wiped.

## 推薦的`軟體`@平台
-  `Conversations`[@]Android
  
[Get Conversations](https://conversations.im/)
- `Dino-im`[@]Linux
  
[Get Dino-im](https://dino.im/)

- `Chat Secure`[@]IOs
  
[Get Chat Secure](https://chatsecure.org/)
  
- `Gajim`[@]Windows (also on BSD, Linux, MacOS)
  
[Get Gajim](https://gajim.org/)
  
- `Beagle IM`[@]MacOs

[Get Beagle IM](https://beagle.im/)



